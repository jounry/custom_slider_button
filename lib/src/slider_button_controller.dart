import 'package:flutter/animation.dart';

class SliderButtonController {
  AnimationController _animationController;

  set animationController(AnimationController value) {
    _animationController = value;
  }

  void setButtonToInitial() {
    _animationController.reverse();
  }

  void setButtonToEnd() {
    _animationController.forward();
  }
}
