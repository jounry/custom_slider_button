import 'package:flutter/material.dart';

import '../slider_button.dart';
import 'shimmer.dart';

class SliderButton extends StatefulWidget {
  ///To make button more customizable add your child widget
  final Widget child;

  ///Sets the radius of corners of a button.
  final double radius;

  ///Use it to define a height and width of widget.
  final double height;
  final double width;
  final double buttonSize;

  ///Use it to define a color of widget.
  final Color backgroundColor;
  final Color baseColor;
  final Color highlightedColor;
  final Color buttonColor;

  ///Change it to gave a label on a widget of your choice.
  final Text label;

  ///Gives a alignment to a slider icon.
  final Alignment alignLabel;
  final BoxShadow boxShadow;
  final Widget icon;
  final Function action;

  ///Make it false if you want to deactivate the shimmer effect.
  final bool shimmer;

  final bool vibrationFlag;

  ///The offset threshold the item has to be dragged in order to be considered
  ///dismissed e.g. if it is 0.4, then the item has to be dragged
  /// at least 40% towards one direction to be considered dismissed
  final double breakPointThreshold;

  /// For controlling the button slider
  final SliderButtonController buttonController;

  /// Duration of animation that take place
  final Duration duration;

  SliderButton({
    @required this.action,
    this.radius = 100,
    this.boxShadow = const BoxShadow(
      color: Colors.black,
      blurRadius: 4,
    ),
    this.child,
    this.vibrationFlag = true,
    this.shimmer = true,
    this.height = 70,
    this.buttonSize = 60,
    this.width = 250,
    this.alignLabel = const Alignment(0.4, 0),
    this.backgroundColor = const Color(0xffe0e0e0),
    this.baseColor = Colors.black87,
    this.buttonColor = Colors.white,
    this.highlightedColor = Colors.white,
    this.buttonController,
    this.duration = const Duration(milliseconds: 200),
    this.label = const Text(
      "Slide to cancel !",
      style: TextStyle(fontWeight: FontWeight.w400, fontSize: 18),
    ),
    this.icon = const Icon(
      Icons.power_settings_new,
      color: Colors.red,
      size: 30.0,
      semanticLabel: 'Text to announce in accessibility modes',
    ),
    this.breakPointThreshold = 1.0,
  }) : assert(buttonSize <= height);

  @override
  _SliderButtonState createState() => _SliderButtonState();
}

class _SliderButtonState extends State<SliderButton> with SingleTickerProviderStateMixin {
  double toDrawValue = 0;
  AnimationController _animationCtrl;
  Animation<double> _tweenAnimation;
  CurvedAnimation _curvedAnimation;
  double maximumDragValue;
  double buttonPaddingLeft;

  @override
  void initState() {
    _animationCtrl = AnimationController(duration: widget.duration, vsync: this);
    _curvedAnimation = CurvedAnimation(
      curve: Curves.easeInOutQuad,
      parent: _animationCtrl,
      reverseCurve: Curves.easeInOutQuad,
    );
    buttonPaddingLeft = (widget.height - widget.buttonSize) / 2;
    widget.buttonController?.animationController = _animationCtrl;

    maximumDragValue = widget.width - widget.buttonSize - (buttonPaddingLeft * 2);
    _tweenAnimation = Tween<double>(begin: 0, end: maximumDragValue).animate(_curvedAnimation);
    _animationCtrl.addListener(() {
      toDrawValue = _tweenAnimation.value;
    });
    _animationCtrl.addStatusListener((AnimationStatus status) {
      if (status == AnimationStatus.completed && toDrawValue >= maximumDragValue) {
        widget.action();
      }
    });

    super.initState();
  }

  @override
  void dispose() {
    _animationCtrl.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(child: _control());
  }

  Widget _control() => Container(
        height: widget.height,
        width: widget.width,
        decoration: BoxDecoration(
          color: widget.backgroundColor,
          borderRadius: BorderRadius.circular(widget.radius),
        ),
        alignment: Alignment.centerLeft,
        child: Stack(
          alignment: Alignment.centerLeft,
          children: <Widget>[
            Container(
              alignment: widget.alignLabel,
              child: widget.shimmer
                  ? Shimmer.fromColors(
                      baseColor: widget.baseColor,
                      highlightColor: widget.highlightedColor,
                      child: widget.label,
                    )
                  : widget.label,
            ),
            Positioned.fill(
              child: GestureDetector(
                onHorizontalDragEnd: (d) {
                  double newAnimatedControllerValue = toDrawValue / maximumDragValue;
                  _animationCtrl.value = newAnimatedControllerValue;

                  if (toDrawValue >= maximumDragValue * widget.breakPointThreshold) {
                    _animationCtrl.forward();
                  } else if (toDrawValue < maximumDragValue * widget.breakPointThreshold) {
                    _animationCtrl.reverse();
                  }
                },
                onHorizontalDragUpdate: (DragUpdateDetails d) {
                  if (d.primaryDelta >= 1.0 || d.primaryDelta <= -1.0) {
                    toDrawValue += d.primaryDelta;
                    bool isToDrawValueExceedMinimum = toDrawValue <= 0;
                    bool isToDrawValueExceedMaximum = toDrawValue >= maximumDragValue;
//
                    if (isToDrawValueExceedMinimum) {
                      toDrawValue = 0;
                    } else if (isToDrawValueExceedMaximum) {
                      toDrawValue = maximumDragValue;
                    }
                    setState(() {});
                  }
                },
                child: AnimatedBuilder(
                    animation: _tweenAnimation,
                    builder: (BuildContext context, Widget child) {
                      return Transform.translate(
                        offset: Offset(toDrawValue, 0),
                        child: Container(
                          width: widget.width - (widget.height),
                          height: widget.height,
                          alignment: Alignment.centerLeft,
                          padding: EdgeInsets.only(
                            left: buttonPaddingLeft,
                          ),
                          child: widget.child ??
                              Container(
                                height: widget.buttonSize,
                                width: widget.buttonSize,
                                decoration: BoxDecoration(
                                  boxShadow: [
                                    widget.boxShadow,
                                  ],
                                  color: widget.buttonColor,
                                  borderRadius: BorderRadius.circular(widget.radius),
                                ),
                                child: Center(child: widget.icon),
                              ),
                        ),
                      );
                    }),
              ),
            ),
          ],
        ),
      );
}
